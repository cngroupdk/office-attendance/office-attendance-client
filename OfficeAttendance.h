
// common and shared definitions

// OfficeAttendance.cpp
void LOG( const std::string& message );
void LOG( const std::wstring& message );

// WinUtils.cpp
std::string FindActualUser ( std::string& errorDesc );
std::string ExecutableDirPath ( void );

// WinService.cpp
bool InstallService ( void );
bool UnInstallService ( void );
bool StartAsService ( void(*funptr)(void) );

bool IsServiceStopRequired ( void );
bool IsServicePaused ( void );

