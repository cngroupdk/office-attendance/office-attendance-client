
#include <windows.h>

#include <iostream>
#include <sstream>
#include <thread>

#include "OfficeAttendance.h"
#include "WinService.h"

void WINAPI cn_ServiceMain ( DWORD dwArgc, LPWSTR *lpszArgv );
void WINAPI cn_ServiceCtrlHandler ( DWORD Opcode );

static CWinService g_thisService;
static CWinServiceHandler g_thisServiceHandler;

// handling command line options:
// -install
// -uninstall
// -service

bool InstallService ( void )
{
	std::wstring errorDesc;
	CWinService::Instance().SetName(L"Office Attendance",L"client for Office Attendance server");
	bool result = CWinService::Instance().Install(errorDesc);
	LOG(errorDesc);
	if(result)
		LOG("Installation: OK");
	else
		LOG("Installation: failure");	

	return result;
}

bool UnInstallService ( void )
{
	std::wstring errorDesc;
	CWinService::Instance().SetName(L"Office Attendance",L"client for Office Attendance server");
	bool result = CWinService::Instance().UnInstall(errorDesc);
	LOG(errorDesc);
	if(result)
		LOG("UnInstallation: OK");
	else
		LOG("UnInstallation: failure");	

	return result;
}

bool StartAsService ( void(*funptr)(void) )
{
	LOG("Service: BEGIN");

	// start service main functionality in separate thread
	std::thread mainWorkingThread(funptr);

	std::wstring errorDesc;
	bool result = CWinService::Instance().Start(errorDesc);	
	
	mainWorkingThread.join();

	LOG(errorDesc);
	LOG("Service: END");
	
	return result;
}

// reporting current status of the service

bool IsServiceStopRequired ( void )
{
	return g_thisServiceHandler.IsStopRequired();
}

bool IsServicePaused ( void )
{
	return g_thisServiceHandler.IsPaused();
}

// handling commands from service manager:
// start, pause, continue, stop

bool CWinServiceHandler::OnStart ( void ) { return true; }
bool CWinServiceHandler::OnPause ( void ) { m_paused=true; return true; }
bool CWinServiceHandler::OnContinue ( void ) { m_paused=false; return true; }
bool CWinServiceHandler::OnStop ( void ) { m_stopRequired=true; return true; }

// CWinService implementation

CWinService::CWinService ( void )
: m_name(L"Office Attendance")
, m_description(L"Office Attendance")
, m_checkPoint(0)
, m_winServiceHandler(&g_thisServiceHandler)
{
}

CWinService::~CWinService ( void )
{
}

CWinService& CWinService::Instance ( void )
{
	return g_thisService;
}

void CWinService::SetName ( const std::wstring& name, const std::wstring& description )
{
	m_name = name;
	m_description = description;
}

bool CWinService::Install ( std::wstring& errorDesc )
{
	LOG(std::wstring(L"Installing service: ")+this->GetName());

	if(m_name.size()==0)
	{
		errorDesc = L"name is empty string";
		return false;
	}

	this->UnInstall(errorDesc);

	SC_HANDLE scManager = OpenSCManager(NULL,NULL,GENERIC_WRITE);
	if(scManager == NULL)
	{
		std::wostringstream ostr;
		ostr << L"OpenSCManager() failed, GetLastError()==" << GetLastError();
		errorDesc = ostr.str();
		return false;
	}

	wchar_t szPath[MAX_PATH]; 
	wchar_t szPath2[MAX_PATH+10]; 
    
    if( !GetModuleFileNameW( NULL, szPath, MAX_PATH ) )
    {
		CloseServiceHandle(scManager);
		std::wostringstream ostr;
		ostr << L"GetModuleFileName() failed, GetLastError()==" << GetLastError();
		errorDesc = ostr.str();
        return false;
    }

	wcscpy(szPath2,L"\"");
	wcscat(szPath2,szPath);
	wcscat(szPath2,L"\" -service");

	LOG(std::wstring(L"creating service: ")+this->GetName());
	LOG(std::wstring(L"  description: ")+this->GetDescription());
	LOG(std::wstring(L"  with path: ")+std::wstring(szPath2));

	SC_HANDLE scService = CreateServiceW( scManager,
										  this->GetName().c_str(), //name,
										  this->GetName().c_str(), //description,
										  GENERIC_ALL,
										  SERVICE_WIN32_OWN_PROCESS,
										  SERVICE_AUTO_START,
										  SERVICE_ERROR_NORMAL,
										  szPath2,
										  NULL,
										  NULL,
										  NULL,
										  NULL,
										  NULL 
										  );

	if(scService == NULL)
	{
		CloseServiceHandle(scManager);
		std::wostringstream ostr;
		ostr << L"CreateService() failed, GetLastError()==" << GetLastError();
		errorDesc = ostr.str();
        return false;
	}

	CloseServiceHandle(scService);
	CloseServiceHandle(scManager);
	
	errorDesc = L"OK";
	return true;;
}

bool CWinService::UnInstall ( std::wstring& errorDesc )
{
	LOG(std::wstring(L"Unnstalling service: ")+this->GetName());

	SC_HANDLE scManager = OpenSCManager(NULL,NULL,GENERIC_WRITE);
	if(scManager == NULL)
	{
		std::wostringstream ostr;
		ostr << L"OpenSCManager() failed, GetLastError()==" << GetLastError();
		errorDesc = ostr.str();
        return false;
	}

	std::wstring wname = this->GetName();

	SC_HANDLE scService = OpenServiceW(scManager,wname.c_str(),GENERIC_ALL);
	if(scService == NULL)
	{
		CloseServiceHandle(scManager);
		
		std::wostringstream ostr;
		ostr << L"OpenService() failed, GetLastError()==" << GetLastError();
		errorDesc = ostr.str();
        return false;
	}	

	if(0 == DeleteService(scService))
	{
		CloseServiceHandle(scService);
		CloseServiceHandle(scManager);

		std::wostringstream ostr;
		ostr << L"DeleteService() failed, GetLastError()==" << GetLastError();
		errorDesc = ostr.str();
        return false;
	}

	CloseServiceHandle(scService);
	CloseServiceHandle(scManager);

	errorDesc = L"OK";
	return true;
}

bool CWinService::Start ( std::wstring& errorDesc )
{
	LOG(std::wstring(L"Starting service: ")+this->GetName());

	if(this->GetName().size()==0)
	{
		errorDesc = L"Service has got no name";
		LOG(errorDesc);
		return false;
	}

	wchar_t wname[1024];
	wcsncpy_s(wname,this->GetName().c_str(),sizeof(wname)/sizeof(wname[0])-1);

	static SERVICE_TABLE_ENTRYW dispatchTable[] =
	{
		//{ serviceName, (LPSERVICE_MAIN_FUNCTION)sp_ServiceMain },
		{ wname, cn_ServiceMain },
		{ NULL, NULL }
	};

	if(!StartServiceCtrlDispatcherW(dispatchTable))
	{
		std::wostringstream ostr;
		ostr << L"StartServiceCtrlDispatcher() failed, GetLastError()==" << GetLastError();
		errorDesc = ostr.str();
        return false;
	}

	errorDesc = L"OK";
	return true;
}

bool CWinService::ReportStatus ( const DWORD dwCurrentState,
                                 const DWORD dwWin32ExitCode,
                                 const DWORD dwWaitHint )
{
	SERVICE_STATUS status;

	status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	status.dwServiceSpecificExitCode = 0;

	if(dwCurrentState == SERVICE_START_PENDING)
		status.dwControlsAccepted = 0;
	else
		status.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE;

	if(dwCurrentState==SERVICE_RUNNING || dwCurrentState==SERVICE_STOPPED)
		status.dwCheckPoint = 0;
	else
		status.dwCheckPoint = m_checkPoint++;

	status.dwCurrentState = dwCurrentState;
	status.dwWin32ExitCode = dwWin32ExitCode;
	status.dwWaitHint = dwWaitHint;

	if(!SetServiceStatus(m_statusHandle,&status)) 
	{
		std::wostringstream ostr;
		ostr << L"SetServiceStatus() failed, GetLastError()==" << GetLastError();
		LOG(ostr.str());
        return false;
	}

	std::wstring statusName;

	switch(dwCurrentState)
	{
		case SERVICE_STOPPED:          statusName = L"SERVICE_STOPPED";          break;
		case SERVICE_START_PENDING:    statusName = L"SERVICE_START_PENDING";    break;
		case SERVICE_STOP_PENDING:     statusName = L"SERVICE_STOP_PENDING";     break;
		case SERVICE_RUNNING:          statusName = L"SERVICE_RUNNING";          break;
		case SERVICE_CONTINUE_PENDING: statusName = L"SERVICE_CONTINUE_PENDING"; break;
		case SERVICE_PAUSE_PENDING:    statusName = L"SERVICE_PAUSE_PENDING";    break;
		case SERVICE_PAUSED:           statusName = L"SERVICE_PAUSED";           break;
		default: statusName = L"UNKNOWN"; break;   
	}

	std::wcout << statusName << std::endl;
	LOG(std::wstring(L"Service status changed to: ") + statusName);

	return true;
}

void WINAPI cn_ServiceMain ( DWORD dwArgc, LPWSTR *lpszArgv )
{
	LOG("cn_ServiceMain()");

	CWinService& service = CWinService::Instance();

	std::wstring wname = CWinService::Instance().GetName();

	service.m_statusHandle = 
		RegisterServiceCtrlHandlerW( wname.c_str(),
									 cn_ServiceCtrlHandler );

	if(!service.m_statusHandle)
	{
		std::ostringstream ostr;
		ostr << "RegisterServiceCtrlHandler() failed, GetLastError()==" << GetLastError();
		LOG(ostr.str());
        return;
	}

	service.ReportStatus(SERVICE_START_PENDING,NO_ERROR,10000);

	bool started = false;

	if(service.m_winServiceHandler)
		started = service.m_winServiceHandler->OnStart();

	if(started)
		service.ReportStatus(SERVICE_RUNNING,NO_ERROR,10000);
	else
		service.ReportStatus(SERVICE_STOPPED,NO_ERROR,10000);

	LOG("cn_ServiceMain() - finish");
}

void WINAPI cn_ServiceCtrlHandler ( DWORD Opcode )
{
	LOG("cn_ServiceCtrlHandler()");

	CWinService& service = CWinService::Instance();

	//DBG_ERROR(0,true,QString("service control: %1").arg(Opcode));

	switch(Opcode)
	{
		case SERVICE_CONTROL_STOP:
		case SERVICE_CONTROL_SHUTDOWN:
			service.ReportStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);
			if(service.m_winServiceHandler)
				if(service.m_winServiceHandler->OnStop())
					service.ReportStatus(SERVICE_STOPPED, NO_ERROR, 0);
			break;

		case SERVICE_CONTROL_PAUSE:
			service.ReportStatus(SERVICE_PAUSE_PENDING, NO_ERROR, 0);
			if(service.m_winServiceHandler)
				if(service.m_winServiceHandler->OnPause());
					service.ReportStatus(SERVICE_PAUSED, NO_ERROR, 0);
			break;

		case SERVICE_CONTROL_CONTINUE:
			service.ReportStatus(SERVICE_CONTINUE_PENDING, NO_ERROR, 0);
			if(service.m_winServiceHandler)
				if(service.m_winServiceHandler->OnContinue())
					service.ReportStatus(SERVICE_RUNNING, NO_ERROR, 0);
			break;

		default:
			break;
	}

	LOG("cn_ServiceCtrlHandler() - finish");
}


