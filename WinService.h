
class CWinServiceHandler
{
private:
	bool m_stopRequired;
	bool m_paused;

public:
	CWinServiceHandler() : m_stopRequired(false),m_paused(false){}

	virtual bool OnStart ( void );
	virtual bool OnPause ( void );
	virtual bool OnContinue ( void );
	virtual bool OnStop ( void );

	bool IsStopRequired ( void ) const { return m_stopRequired; }
	bool IsPaused ( void ) const { return m_paused; }
};

class CWinService
{
private:

	static bool m_singletonInitialized;

	std::wstring m_name;
	std::wstring m_description;

	DWORD                  m_checkPoint;
	SERVICE_STATUS_HANDLE  m_statusHandle;

	CWinServiceHandler   * m_winServiceHandler;

	bool ReportStatus ( const DWORD dwCurrentState,
						const DWORD dwWin32ExitCode,
						const DWORD dwWaitHint );

public:

	enum EStatus
	{
		_Stopped,
		_StartPending,
		_StopPending,
		_Running,
		_ContinuePending,
		_PausePending,
		_Paused,
	};

	 CWinService ( void );
	~CWinService ( void );

	static CWinService& Instance ( void );

	void SetName ( const std::wstring& name, const std::wstring& description = L"" ); 

	bool Install ( std::wstring& errorDesc );
	bool UnInstall ( std::wstring& errorDesc );	

	bool Start ( std::wstring& errorDesc );

	void SetServiceHandler ( CWinServiceHandler * serviceHandler ) { m_winServiceHandler = serviceHandler; } 

	EStatus GetStatus ( void ) const;
	std::wstring GetName ( void ) const { return m_name; }
	std::wstring GetDescription ( void ) const { return m_description; }

friend void WINAPI cn_ServiceMain ( DWORD dwArgc, LPWSTR *lpszArgv );
friend void WINAPI cn_ServiceCtrlHandler ( DWORD Opcode );
};


