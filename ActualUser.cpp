
// WINDOWS only source code
// implmenting FindActualUser()

#include <windows.h>
#include <psapi.h>

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <set>
#include <vector>
#include <algorithm>

#include "OfficeAttendance.h"

// Windows spocific code:
// functions:
//   FindActualUser
//    

#define MAX_NAME 256

std::string FindActualUser ( std::string& errorDesc )
{
	std::string result = "unknown";

    std::set<std::string> allUsers;

    DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;

    if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
    {
        errorDesc = "EnumProcesses() failed";
        return "";
    }

    // Calculate how many process identifiers were returned.

    cProcesses = cbNeeded / sizeof(DWORD);
    //std::cout << "total number of processes: " << cProcesses << std::endl;

    bool error = false;

    for ( i = 0; i < cProcesses; i++ )
    {
        if( aProcesses[i] == 0 )
        	continue;

        HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                                       FALSE,
									   aProcesses[i] );

        if(!hProcess)
        	continue;

        HANDLE hToken = NULL;

        if( !OpenProcessToken( hProcess, TOKEN_QUERY, &hToken ) )
        {
            CloseHandle( hProcess );
            continue;
        }

        error = true;

        if( hToken )
        {
        	DWORD dwLength = 0;
        	PTOKEN_USER ptu = NULL;

            if(!GetTokenInformation ( hToken,         // handle to the access token
            						  TokenUser,      // get information about the token's groups
									  (LPVOID) ptu,   // pointer to PTOKEN_USER buffer
									  0,              // size of buffer
									  &dwLength ) )   // receives required buffer size
            {
            	if(GetLastError() == ERROR_INSUFFICIENT_BUFFER)
            	{
            		ptu = (PTOKEN_USER)HeapAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,dwLength);
            		if(ptu != NULL)
            		{
            			if( GetTokenInformation( hToken,         // handle to the access token
            				   	   	   	   	     TokenUser,      // get information about the token's groups
            		                             (LPVOID) ptu,   // pointer to PTOKEN_USER buffer
            		                             dwLength,       // size of buffer
            		                             &dwLength ))    // receives required buffer size
            			{
            			    SID_NAME_USE SidType;
            			    char lpName[MAX_NAME];
            			    char lpDomain[MAX_NAME];

            			    DWORD dwSize = MAX_NAME;

            			    if( LookupAccountSidA( NULL , ptu->User.Sid, lpName, &dwSize, lpDomain, &dwSize, &SidType ) )
            			    {
                                if(    strcmp(lpDomain,"Font Driver Host")
                                    && strcmp(lpDomain,"NT AUTHORITY")
                                    && strcmp(lpDomain,"Window Manager")
                                  ) 
                                {
            			            //std::cout << "USER: " << lpName << ", DOMAIN: " << lpDomain << std::endl;
                                    //std::string newUser = std::string(lpDomain) + "\\" + lpName;
                                    std::string newUser = lpName;
                                    
                                    if(0==allUsers.count(newUser))
                                        allUsers.insert(newUser);
                                }

                                error = false;
            			    }
            			}
            		}
            	}
            }
        }

        if(error)
        {
            std::ostringstream ostr;
            ostr << "FindActualUser() Error: " << GetLastError();
            errorDesc = ostr.str();
        }

        CloseHandle( hProcess );
        CloseHandle( hToken );

        if(error)
            return "";
    }

    result = "";
    std::set<std::string>::const_iterator iter;
    for( iter = allUsers.cbegin();
         iter != allUsers.cend(); 
         ++iter )
    {
        std::string userName = *iter;
        if(result.size()>0) result.append(";");
        result.append(userName);
    }

    LOG("FindActualUser : ");
    LOG(result);

    errorDesc = "OK";
	return result;
}

std::string ExecutableDirPath ( void )
{
    std::vector<char> pathBuf; 
    DWORD copied = 0;
    do {
        pathBuf.resize(pathBuf.size()+MAX_PATH);
        copied = GetModuleFileName(0, &pathBuf.at(0), pathBuf.size());
    } while( copied >= pathBuf.size() );

    pathBuf.resize(copied);
    std::string path(pathBuf.begin(),pathBuf.end());

    int pos = path.find_last_of('\\');
    if(pos>=0)
        path = path.substr(0,pos+1);

    return path;
}






