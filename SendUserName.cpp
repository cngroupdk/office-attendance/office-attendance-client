
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <cstdlib>
#include <iostream>
#include <string>

#include "OfficeAttendance.h"

namespace beast = boost::beast;     // from <boost/beast.hpp>
namespace http = beast::http;       // from <boost/beast/http.hpp>
namespace net = boost::asio;        // from <boost/asio.hpp>
using tcp = net::ip::tcp;           // from <boost/asio/ip/tcp.hpp>

// constants, maybe parametrized in future
/*
#define HOST "127.0.0.1"
#define PORT "8080"
#define TARGET "/office-attendances"
#define USER_AGENT_STRING "Attendance System Client"
*/

#define HOST "172.28.70.27"
#define PORT "8080"
#define TARGET "/office-attendances"
#define USER_AGENT_STRING "Attendance System Client"

bool SendUserName ( const std::string& userName, std::string& errorDesc )
{
    bool result = true;

    try
    {
        auto const host = HOST;
        auto const port = PORT;
        auto const target = TARGET;
        const int version = 11;

        // The io_context is required for all I/O
        net::io_context ioc;

        // These objects perform our I/O
        tcp::resolver resolver(ioc);
        beast::tcp_stream stream(ioc);

        // Look up the domain name
        auto const results = resolver.resolve(host, port);

        //stream.expires_after(std::chrono::seconds(30));
        // Make the connection on the IP address we get from a lookup
        stream.connect(results);  // TODO: check results

        // Set up an HTTP GET request message
        http::request<http::string_body> req{http::verb::post, target, version};
        req.set(http::field::host, host);
        req.set(http::field::user_agent, USER_AGENT_STRING);
        
        //req.set(beast::http::field::content_type, "application/x-www-form-urlencoded");
        //req.body() = std::string("UserName=") + userName;
        req.set(beast::http::field::content_type, "application/json");
        req.body() = std::string("{\r\n    \"userId\": \"") + userName + std::string("\"\r\n}\r\n");
        
        req.prepare_payload();

        // Send the HTTP request to the remote host
        http::write(stream, req);

        // This buffer is used for reading and must be persisted
        beast::flat_buffer buffer;

        // Declare a container to hold the response
        http::response<http::dynamic_body> res;

        // Receive the HTTP response
        http::read(stream, buffer, res);

        // Write the message to standard out
        std::cout << res << std::endl;
        std::cout << "RESULT CODE is: " << res.result_int() << std::endl;

        if(res.result_int() != 200 && res.result_int() != 201)
        {
            result = false;
            std::ostringstream str;
            str << "Wrong result code: " << res.result_int();
            errorDesc = str.str();
        }

        // Gracefully close the socket
        beast::error_code ec;
        stream.socket().shutdown(tcp::socket::shutdown_both, ec);

        // not_connected happens sometimes
        // so don't bother reporting it.
        //
        if(ec && ec != beast::errc::not_connected)
            throw beast::system_error{ec};

        // If we get here then the connection is closed gracefully
    }
    catch(std::exception const& e)
    {
        //std::cerr << "Error: " << e.what() << std::endl;
        errorDesc = e.what();
        return false;
    }

    if(result)
        errorDesc = "OK";

    return result;
}



