
#include <iostream>
#include <fstream>
#include <filesystem>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "OfficeAttendance.h"

bool SendUserName ( const std::string& userName, std::string& errorDesc );
bool RegisterUser ( const boost::system::error_code& e, boost::asio::deadline_timer* t, int* count );
bool DoPause ( const boost::system::error_code& e, boost::asio::deadline_timer* t, int* count );
void MainLoop ( void );

//void DBG ( const std::string& message, const std::string& errorDesc = "" );

int main(int argc, char *argv[])
{
  LOG("");
  LOG("Office Attendance is starting ");
  LOG("");

  // for WINDOWS only
  if(argc == 2 && !strcmp(argv[1],"-install"))
  {
    LOG("with -install");
    bool result = InstallService();
    return result ? EXIT_SUCCESS : EXIT_FAILURE;
  }
  
  if(argc == 2 && !strcmp(argv[1],"-uninstall"))
  {
    LOG("with -uninstall");
    bool result = UnInstallService();
    return result ? EXIT_SUCCESS : EXIT_FAILURE;
  }

  if(argc == 2 && !strcmp(argv[1],"-service"))
  {
    LOG("with -service");
    bool result = StartAsService(MainLoop);
    return result ? EXIT_SUCCESS : EXIT_FAILURE;
  }
  
  MainLoop();

  return 0;
}

bool DoPause ( const boost::system::error_code& e, boost::asio::deadline_timer* t, int* count)
{
  // during PAUSE state of the service
  // do nothing
  return true;
}

bool RegisterUser ( const boost::system::error_code& e, boost::asio::deadline_timer* t, int* count )
{
  //LOG("RegisterUser()");

  std::string errorDesc = "OK";

  static boost::posix_time::ptime registrationTime(boost::posix_time::min_date_time);

  boost::posix_time::ptime actualTime = boost::posix_time::second_clock::local_time();
  //std::cout << "actual time: " << to_simple_string(actualTime) << std::endl;
  //std::cout << "last successful registration time: " << to_simple_string(registrationTime) << std::endl;

  if(    actualTime-registrationTime < boost::posix_time::time_duration(24,0,0,0)
      && actualTime.date().day() == registrationTime.date().day() )
  {
    LOG("user today already registered");
    return true;
  }

  if(actualTime.time_of_day().hours() < 6 || actualTime.time_of_day().hours() >= 18)
  {
    LOG("do not perform registration operation out of time: 06:00 - 18:00");
    return true;
  }

  std::string actualUser = FindActualUser(errorDesc);
  if(actualUser.size()==0)
  {
    std::ostringstream os;
    os << "unknown actual user: " << errorDesc;
    LOG(os.str());
    return false;
  }

  //LOG("actual user:");
  //LOG(actualUser);

  if(!SendUserName(actualUser,errorDesc))
  {
    std::ostringstream os;
    os << "unknown actual user: " << errorDesc;
    LOG(os.str());
    return false;
  }

  LOG("REGISTRATION SUCCESS for:");
  LOG(actualUser);
  
  registrationTime = actualTime;

  return true;
}

// application main loop

void MainLoop ( void )
{
  for(int counter=0;;++counter)
  {
    //LOG("MAIN LOOP");

    if(IsServiceStopRequired())
      break;

    boost::asio::io_service io;
    int count = counter;
    boost::system::error_code ec;

    boost::asio::deadline_timer t(io, boost::posix_time::seconds(10));  // NOTE: 

    if(IsServicePaused())
    {
      t.async_wait(boost::bind(DoPause,ec, &t, &count));
      io.run();
    }
    else
    {
      t.async_wait(boost::bind(RegisterUser,ec, &t, &count));
      io.run();
    }
  }

  LOG("END of MainLoop()");
}

// logging

#define LOGFILENAME  "OfficeAttendance_0.txt"
#define LOGFILENAME2 "OfficeAttendance_1.txt"

static std::mutex g_logMutex;

static void MaintainLog ( void )
{
  try
  {
    std::string executabledirpath = ExecutableDirPath();

    int size = std::filesystem::file_size(executabledirpath+LOGFILENAME);
  
    if(size < 1000000)
      return;

    std::filesystem::rename(executabledirpath+LOGFILENAME,executabledirpath+LOGFILENAME2);
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }
  
  //fs << L"FILE SIZE: " << size << std::endl;
}

void LOG( const std::string& message )
{
  std::lock_guard<std::mutex> lck(g_logMutex);

  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ofstream fs;
  fs.open(ExecutableDirPath()+LOGFILENAME,std::ios::app);

  //std::string errorDesc;
  //fs << "executable dir path: " << ExecutableDirPath(errorDesc) << std::endl;
  
  fs << std::put_time(&tm, "%Y-%m-%d %H:%M:%S : ") << message << std::endl;
  fs.close();
  std::cout << message << std::endl;
  MaintainLog();
}

void LOG( const std::wstring& message )
{
  std::lock_guard<std::mutex> lck(g_logMutex);

  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::wofstream fs;
  fs.open(ExecutableDirPath()+LOGFILENAME,std::ios::app);
  //fs << message << std::endl;
  fs << std::put_time(&tm, L"%Y-%m-%d %H:%M:%S : ") << message << std::endl;
  fs.close();
  std::wcout << message << std::endl;
  MaintainLog();
}

















